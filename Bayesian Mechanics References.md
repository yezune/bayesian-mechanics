Bayesian Mechanics(BM)의 주요 논문, 책등 저작은 다음과 같습니다.

**논문**

- **Jaynes, E. T. (1957). Information theory and statistical mechanics. Physical Review, 106(4), 620-630.**
- **Jaynes, E. T. (1963). Prior probabilities. In R. D. Rosenkrantz (Ed.), The foundations of statistical inference (pp. 17-52). Pittsburgh, PA: University of Pittsburgh Press.**
- **Barrett, J. A. (1999). Quantum mechanics as a language. Foundations of Physics, 29(1), 1323-1336.**
- **Barrett, J. A., Byrne, P., Kent, A., & Wallace, D. (2005). No signaling and quantum key distribution. Physical Review A, 72(1), 022301.**
- **Caves, C. M., Fuchs, C. A., & Schack, R. (2002). Quantum probabilities as Bayesian probabilities. Physical Review A, 65(2), 022305.**
- **Fuchs, C. A., & Mermin, N. D. (2003). The quantum state of classical probability. Foundations of Physics, 33(5), 755-775.**
- **Fuchs, C. A., & Schack, R. (2013). Quantum-Bayesian coherence. Review of Modern Physics, 85(2), 169-214.**
- **Leifer, M. S. (2014). Quantum Bayesianism. In E. N. Zalta (Ed.), The Stanford encyclopedia of philosophy (Fall 2014 edition). Metaphysics Research Lab, Stanford University.**

**책**

- **Barrett, J. A. (2011). The quantum Bayesian view of probability. In D. Dieks & P. Vermaas (Eds.), Explanation, prediction, and confirmation (pp. 211-256). Oxford, UK: Oxford University Press.**
- **Fuchs, C. A. (2016). Quantum foundations. In E. N. Zalta (Ed.), The Stanford encyclopedia of philosophy (Winter 2016 edition). Metaphysics Research Lab, Stanford University.**
- **Schack, R. (2017). Quantum Bayesianism. In J. Butterfield & C. Pagonis (Eds.), The Oxford handbook of philosophy of physics (pp. 273-295). Oxford, UK: Oxford University Press.**
